package me.yatwong.hbaseDemo

import java.io.IOException

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.TableName
import org.apache.hadoop.hbase.client.{ColumnFamilyDescriptorBuilder, ConnectionFactory, Get, TableDescriptorBuilder}
import org.apache.hadoop.hbase.util.Bytes
import org.slf4j.{Logger, LoggerFactory}

import scala.collection.JavaConversions._
import scala.collection.immutable.HashMap

object HbaseScalaDemo{
}

class HbaseScalaDemo private(conf:Configuration){
  private val log:Logger  = LoggerFactory.getLogger(HbaseScalaDemo.getClass)

  val connect_option = try{
    val r = ConnectionFactory.createConnection(conf)
    Option(r)
  }catch{
    case e: IOException => log.error("连接hbase失败")
      None
  }

  def creatTable(tableName:String, columnFamily:List[String]): Boolean ={
    try{
      val admin = connect_option.get.getAdmin
      val familyDesriptors = columnFamily.map{cf =>
        ColumnFamilyDescriptorBuilder.newBuilder(Bytes.toBytes(cf)).build()
      }
      val tabaleDesriptor = TableDescriptorBuilder.newBuilder(TableName.valueOf(tableName))
        .setColumnFamilies(asJavaCollection(familyDesriptors))
        .build()
    }catch{
      case e => log.error(s"创建表失败")
        return false
    }finally {
      //关闭表
    }
    return true
  }

  def getRowData(tableName:String, rowKey:String): Option[Map[String, String]] ={
    try{
      val hbaseRes = connect_option.get.getTable(TableName.valueOf(tableName))
        .get(new Get(Bytes.toBytes(rowKey)))
      if(hbaseRes == null && !hbaseRes.isEmpty){
        val res = hbaseRes.listCells().map{ cell =>
          (Bytes.toString(cell.getQualifierArray(), cell.getQualifierOffset(), cell.getQualifierLength())
            ,Bytes.toString(cell.getValueArray(), cell.getValueOffset(), cell.getValueLength()))
        }.toMap
        Option(res)
      }else{
        None
      }
    }catch {
      case e => log.error(s"查询数据失败，表名 $tableName ,key $rowKey")
        None
    }
  }
}
















