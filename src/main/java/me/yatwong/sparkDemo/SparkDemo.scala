package me.yatwong.sparkDemo


import org.apache.spark.{SparkConf, SparkContext}

object SparkDemo {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("sparkStreamingDemo")
      .setMaster("local[2]")
    val sc = new SparkContext(conf)
    val res = sc.parallelize(1 to 10000).reduce(_+_)


    val spark = SparkSession.builder.appName("Simple Application").getOrCreate()
    val wordCounts = textFile.flatMap(line => line.split(" ")).groupByKey(identity).count()
    println(s"==========$res===========")
  }
}
