package me.yatwong.sparkDemo

import org.apache.spark.{SparkConf, SparkContext}

object SparkStandaloneDemo {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("SparkStandaloneDemo")
      .setMaster("spark://10.0.20.22:7077")
      .setJars(Array("/home/sunyw/IdeaProjects/sparkdemo/out/artifacts/sparkdemo/sparkdemo.jar"))

    val sc = new SparkContext(conf)
    val res = sc.parallelize(1 to 10000).reduce(_+_)

    println(res)

  }
}
