package me.yatwong.sparkDemo

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.streaming.{Seconds, StreamingContext}

object SparkStreamingDemo {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("sparkStreamingDemo")
      .setMaster("local[2]")
    val sc = new SparkContext(conf)
    val ssc = new StreamingContext(sc, Seconds(1))



    ssc.start()
    ssc.awaitTermination()
  }
}
