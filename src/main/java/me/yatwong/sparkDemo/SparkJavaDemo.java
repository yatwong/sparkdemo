package me.yatwong.sparkDemo;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.SparkSession;

import java.util.Iterator;

public class SparkJavaDemo {
    static void insertItertorToHbase(Iterator it) {
        while(!it.hasNext()){System.out.println(it.next());}
    }

    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println("Usage: JavaWordCount <file>");
            System.exit(1);
        }

        SparkSession spark = SparkSession
                .builder()
                .appName("JavaWordCount")
                //.master("local[*]")
                .getOrCreate();

        JavaRDD<String> lines = spark.read().textFile(args[0]).javaRDD();

        lines.foreachPartition(iterator -> insertItertorToHbase(iterator));

        spark.stop();
    }

}
